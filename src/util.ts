//Typescript types
import { FileOrFolder } from "./types";

/**
 * Counts the number of files in a given folder structure
 */
export const countTotalFiles = (folderStructure: FileOrFolder[]) => {
  let fileCount = 0;
  
  for(let fileOrFolder of folderStructure) {
    if(fileOrFolder.type === "file") {
      fileCount++;
    }
    else if(fileOrFolder.type === "folder") {
      fileCount += countTotalFiles(fileOrFolder.children);
    }
    else {
      //Unexpected item type, exclude from file count
    }
  }
  return fileCount;
}

/**
 * Returns the sum of the sizes of all the files in the folder structure
 */
export const countTotalFileSize = (folderStructure: FileOrFolder[]) => {
  let fileSize = 0;
  
  for(let fileOrFolder of folderStructure) {
    if(fileOrFolder.type === "file") {
      fileSize += fileOrFolder.size;
    }
    else if(fileOrFolder.type === "folder") {
      fileSize += countTotalFileSize(fileOrFolder.children);
    }
    else {
      //Unexpected item type, exclude from file count
    }
  }
  return fileSize;
}

const filesizePrefixMap = [
  {exponent: 0, prefix: "B"},
  {exponent: 1, prefix: "KB"},
  {exponent: 2, prefix: "MB"},
  {exponent: 3, prefix: "GB"},
  {exponent: 4, prefix: "TB"}
];

/**
 * Takes a filesize in bytes and returns a formatted string with the relavent filesize prefix
 */
export const formatFileSize = (fileSize: number) => {
  let roundedFileSize = Math.round(fileSize);
  let exponent = 0;
  
  //Divide the file size by 1000 until the size is less than 1000 and keep track of exponent
  while(roundedFileSize.toString().length > 3) {
    roundedFileSize = Math.round(roundedFileSize / 1000);
    exponent++;
  }
 
  //Generate the file size string
  const prefixObject = filesizePrefixMap.find((prefixObject) => prefixObject.exponent === exponent);
  if(prefixObject) {
    return roundedFileSize + " " + prefixObject.prefix;
  }

  //No matching prefix in the prefix list, handle by returning the value in Bytes
  return Math.pow(1000, exponent) + roundedFileSize + " B"; 
}