//The typescript types used throughout the project

export type FileOrFolder = File | Folder;

export type Folder = {
    type: "folder",
    name: string,
    children: FileOrFolder[]
}

export type File = {
    type: "file",
    name: string,
    size: number
}