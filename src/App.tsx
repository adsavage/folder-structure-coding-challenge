import React, { useEffect, useState } from 'react';
import './App.css';
import loadingSpinner from "./img/loading-spinner.gif";

//React Components
import FoldersAndSummaryContainer from "./components/FoldersAndSummaryContainer";

//Typescript types
import { FileOrFolder } from "./types"

//URI of the folder structure API
const API_URI = "https://chal-locdrmwqia.now.sh/";

/**
 * Top-level component of the app
 */
const App: React.FC = () => {
  const [isDataFetchingError, setIsDataFetchingError] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [folderStructure, setFolderStructure] = useState<FileOrFolder[]>([]);

  async function fetchFolders() {
    setIsLoading(true);

    const res = await fetch(API_URI);
    res.json()
      .then(res => { 
        if(res.data) {
          setIsDataFetchingError(false);
          setFolderStructure(res.data); 
          setIsLoading(false);
        }
        else {
          setIsDataFetchingError(true);
          setIsLoading(false);
        }
      })
      .catch(_ => {setIsDataFetchingError(true); setIsLoading(false)});
  }

  useEffect(() => {
    fetchFolders();
  }, [])
  
  return (
    <div className="App">
      {
        isLoading 
          ? <img src={loadingSpinner} alt="Loading" />
          : isDataFetchingError
            ? <p>Unable to retrive folder data</p>
            : <FoldersAndSummaryContainer folderStructure={folderStructure} />    
      }
    </div>
  );
}

export default App;
