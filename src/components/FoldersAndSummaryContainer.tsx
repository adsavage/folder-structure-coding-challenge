import React from 'react';
import '../App.css';

//React components
import FolderStructure from "./FoldersAndSummaryComponents/FolderStructure";
import SummaryTotals from "./FoldersAndSummaryComponents/SummaryTotals";

//Typescript types
import { FileOrFolder } from "../types"

//Utility Functions
import { countTotalFiles, countTotalFileSize } from "../util";

/**
 * Container component for the page displaying the folder structure and summary
 */
const FoldersAndSummaryContainer: React.FC<{folderStructure: FileOrFolder[]}> = (props) => {
  return (
    <React.Fragment>
      <div className="folder-structure-container">
        <FolderStructure folderStructure={props.folderStructure} />
      </div>
      <div className="summary-totals-container">
        <SummaryTotals totalFiles={countTotalFiles(props.folderStructure)} totalFilesize={countTotalFileSize(props.folderStructure)} />
      </div>
    </React.Fragment>
  );
}

export default FoldersAndSummaryContainer;