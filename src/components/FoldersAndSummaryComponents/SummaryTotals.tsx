import React from 'react';
import '../../App.css';

//Utility functions
import { formatFileSize } from "../../util";

/**
 * Displays the total file count and total size count below the file structure
 */
const SummaryTotals: React.FC<{totalFiles: number, totalFilesize: number}> = (props) => {
  return (
    <div className="summary-totals-container">
      <p className="summary-text">Total Files: {props.totalFiles}</p>
      <p className="summary-text">Total Filesize: {formatFileSize(props.totalFilesize)}</p>
    </div>
  );
}

export default SummaryTotals;