import React from 'react';
import '../../App.css';

//React components
import FileRow from "./FolderStructureComponents/FileRow";
import FolderRow from "./FolderStructureComponents/FolderRow";

//Utility functions
import { FileOrFolder } from "../../types"

/**
 * Displays the folder structure with a list of files and folders
 */
const FolderStructure: React.FC<{folderStructure: FileOrFolder[]}> = (props) => {

  return (
    <div className="folder-structure">
      {
        props.folderStructure.map((fileOrFolder: FileOrFolder, index: number) => {
          if(fileOrFolder.type === "folder") {
            return <FolderRow folderName={fileOrFolder.name} children={fileOrFolder.children} key={index} />
          }
          else {
            return <FileRow fileName={fileOrFolder.name} fileSize={fileOrFolder.size} key={index} />
          } 
        })
      }
        
    </div>
  );
}

export default FolderStructure;