import React, { useState } from "react";
import "../../../App.css";

//Images
import folderFilledIcon from "../../../img/folder-filled-icon.png";
import folderUnfilledIcon from "../../../img/folder-unfilled-icon.png";
import chevronRightIcon from "../../../img/chevron-right-icon.png";

//React Components
import FolderStructure from "../FolderStructure";

//Typescript types
import { FileOrFolder } from "../../../types";

/** 
 *  Displays a row for a folder with the capability to expand the folder and view the contents
*/
const FolderRow: React.FC<{folderName: string, children: FileOrFolder[]}> = (props) => {
  const [isOpen, setIsOpen] = useState(false);

  return (
    <div className="folder-group">
      <div className="folder-row">
        <img 
          className={props.children.length > 0 ? isOpen ? "chevron-icon-open" : "chevron-icon-closed" : "chevron-icon-hidden"} 
          src={chevronRightIcon} alt="Right Chevron Icon" 
          onClick={() => setIsOpen(!isOpen)} 
        />
        <img className="folder-icon" src={isOpen ? folderUnfilledIcon : folderFilledIcon} alt="Closed Folder Icon" />
        <span>{props.folderName}</span>
      </div>
      {
        isOpen ? <FolderStructure folderStructure={props.children} /> : null
      }
    </div>
  );
}

export default FolderRow;