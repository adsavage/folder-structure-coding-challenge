import React from 'react';
import '../../../App.css';

//Images
import fileIcon from '../../../img/file-icon.png';

//Utility functions
import { formatFileSize } from "../../../util";

/**
 * Displays a row for a file with the file name and size
 */

const FileRow: React.FC<{fileName: string, fileSize: number}> = (props) => {
  return (
    <div className="file-row">
      <img className="file-icon" src={fileIcon} alt="File Icon" />
      <span className="file-name">{props.fileName}</span>
      <span className="file-size">{formatFileSize(props.fileSize)}</span>
    </div>
  );
}

export default FileRow;