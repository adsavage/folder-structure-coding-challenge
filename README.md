# Folder-Structure-Coding-Challenge
This is Alex Savages coding challenge for Be Collective. A deployed version of this code can be found running here: [http://http://ec2-13-211-132-43.ap-southeast-2.compute.amazonaws.com:5000/](http://ec2-13-211-132-43.ap-southeast-2.compute.amazonaws.com:5000/).

## Implementation Notes

 - Frameworks: This project uses React and Typescript, create-react-app was used to generate a project scaffold